Build the **wyrm-hub** with
```bash
sudo su
bash <(curl https://gitlab.com/wyrm-os/wyrm-hub/raw/master/build-wyrm-hub)
```